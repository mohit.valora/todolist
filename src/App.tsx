import React from 'react';
import './App.css';
import ReactRouter from './Router';

function App() {
  return (
    <>
    <ReactRouter/>
    </>
  );
}

export default App;
