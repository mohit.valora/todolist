import React from "react";
import { Route, Routes } from "react-router-dom";
import { Home } from "../components/home/Home";


function ReactRouter() {
    return (
      <>
        <Routes>
          <Route path='/' element={<Home/>}/>
        </Routes>
      </>
    );
  }
  
  export default ReactRouter;