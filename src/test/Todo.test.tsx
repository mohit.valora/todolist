import React from "react";
import { render, screen, fireEvent } from "@testing-library/react"
import { Todo } from "../components/TodoFiles/Todo";


describe("Todo", () => {

    const AddTask = (tasks: string[]) => {
        const TodoInput = screen.getByPlaceholderText("Add a new task here ...")
        const TodoButton = screen.getByRole("button")
        tasks.forEach(task => {
            fireEvent.change(TodoInput, { target: { value: task } })
            fireEvent.click(TodoButton)
        })
    }

    it("WHEN add task  THEN render in input filed", async () => {
        render(<Todo />)
        AddTask(["Go Grocee"])
        const listElemant = screen.getByText(/Go Grocee/i)
        expect(listElemant).toBeInTheDocument()
    });

    it("maltiple task add in list", async () => {
        render(<Todo />)
        AddTask(["Go Grocee", "second", "last Change"])
        const listElemants = screen.getAllByTestId("listElement")
        expect(listElemants.length).toBe(3)
    });

    it("null task not add in list", async () => {
        render(<Todo />)
        AddTask(["Go Grocee", "", "last Change"])
        const listElemants = screen.getAllByTestId("listElement")
        expect(listElemants.length).toBe(2)
    });

    it("click on list item throw line", async () => {
        render(<Todo />)
        AddTask(["Go Grocee"])
        const listElemant = screen.getByText(/Go Grocee/i)
        fireEvent.click(listElemant)
        expect(listElemant).toHaveStyle('text-decoration:line-through')
    });
})