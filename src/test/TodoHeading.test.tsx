import React from "react";
import { render, screen } from "@testing-library/react"
import { TodoHeading } from "../components/TodoFiles/TodoHeading";

it("WHEN pass string name in heading THEN heading should be passed", async () => {
    render(<TodoHeading title="title" />)
    const Todoheading = screen.getByText(/title/i);
    expect(Todoheading).toBeInTheDocument();
});



it("ROlE of todoHeading", async () => {
    render(<TodoHeading title="title" />)
    const Todoheading = screen.getByRole("heading");
    expect(Todoheading).toBeInTheDocument();
});


it("TITLE of todoHeading", async () => {
    render(<TodoHeading title="title" />)
    const Todoheading = screen.getByTitle("heading");
    expect(Todoheading).toBeInTheDocument();
});


it("DATA_TESTID of todoHeading", async () => {
    render(<TodoHeading title="title" />)
    const Todoheading = screen.getByTestId("heading");
    expect(Todoheading).toBeInTheDocument();
});



it("FINDBY todoHeading", async () => {
    render(<TodoHeading title="heading" />)
    const Todoheading = await screen.findByText(/heading/i);
    expect(Todoheading).toBeInTheDocument();
});


it("QUERYBY todoHeading", async () => {
    render(<TodoHeading title="heading" />)
    const Todoheading = screen.queryByText("heading");
    expect(Todoheading).toBeInTheDocument();
});



it("QUERYBY todoHeading", async () => {
render(<TodoHeading title="not same" />)
    const Todoheading = screen.queryByText("heading");
    expect(Todoheading).not.toBeInTheDocument();
});


it("getAllBy todoHeading", async () => {
render(<TodoHeading title="not same" />)
    const Todoheadings = screen.getAllByRole("heading");
    expect(Todoheadings.length).toBe(1);
});