import React from "react";
import { render, screen, fireEvent } from "@testing-library/react"
import { TodoFooter } from "../components/TodoFiles/TodoFooter";
import { Todo } from "../components/TodoFiles/Todo";


describe("TodoFooter", () => {

    const AddTask = (tasks: string[]) => {
        const TodoInput = screen.getByPlaceholderText("Add a new task here ...")
        const TodoButton = screen.getByRole("button")
        tasks.forEach(task => {
            fireEvent.change(TodoInput, { target: { value: task } })
            fireEvent.click(TodoButton)
        })
    }

    it("WHEN add task  THEN add numbers of lefts task increment", async () => {
        render(<TodoFooter leftTask={0} />)
        const Todoheading = screen.getByText(/0 tasks left/i);
        expect(Todoheading).toBeInTheDocument();
    });
    
    it("WHEN add only 1 task  THEN add 1 left task ", async () => {
        render(<TodoFooter leftTask={1} />)
        const Todoheading = screen.getByText(/1 task left/i);
        expect(Todoheading).toBeTruthy();
    });

    it("When Add new task increase in footer task", async () => {
        render(<Todo />)
        AddTask(["add ivent"])
        const Todoheading = screen.getByText(/1 task left/i);
        expect(Todoheading).toBeInTheDocument();
    })

    it("When Add new task and click on list than left 0 task ", async () => {
        render(<Todo />)
        AddTask(["add ivent"])
        const TaskList=screen.getByTestId("listElement")
        fireEvent.click(TaskList)
        const Todoheading = screen.getByText(/0 tasks left/i);
        expect(Todoheading).toBeInTheDocument();
    })
})