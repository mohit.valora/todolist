import React from "react";
import { render, screen, fireEvent } from "@testing-library/react"
import { TodoInput } from "../components/TodoFiles/TodoInput";


const MokInputList = jest.fn()
describe("AddInput", () => {

    it("WHEN add task  THEN render in input filed", async () => {
        render(<TodoInput inputLists={[""]} setInputLists={MokInputList} />)
        const TodoInputList = screen.getByPlaceholderText("Add a new task here ...")
        expect(TodoInputList).toBeInTheDocument()
    });

    it("should be able to type in input", async () => {
        render(<TodoInput inputLists={[""]} setInputLists={MokInputList} />)
        const TodoInputElement = screen.getByPlaceholderText(/Add a new task here .../i)
        fireEvent.change(TodoInputElement, { target: { value: "Go Grocery Shoping" } })
        expect(TodoInputElement).toHaveValue("Go Grocery Shoping");
    });

    it("should have empty input filed when add buttn is clicked", async () => {
        render(<TodoInput inputLists={[""]} setInputLists={MokInputList} />)
        const TodoInputElement = screen.getByPlaceholderText(/Add a new task here .../i)
        fireEvent.change(TodoInputElement, { target: { value: "Go Grocery Shoping" } })
        const AddButton = screen.getByRole("button")
        fireEvent.click(AddButton)
        expect(TodoInputElement).toHaveValue("");
    });
})