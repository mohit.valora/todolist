import React, { useEffect, useState } from 'react';
import axios from "axios";
import { response } from 'msw';
import { type } from '@testing-library/user-event/dist/type';

type note ={
    id:number,
    title:string,
    desc:string,
};
type notes=note[]

const NoteList = () => {
const [notes,setNotes] =useState<notes|null>(null)

    useEffect(()=>{
        axios.get("/notes-list").then((response)=>{
            const {data}=response;
            const {notes}=data;
            setNotes(notes)
            
        })
    },[])

    if(notes===null){
        return<div data-testid="loader" >Loading....</div>
    }
    return (
        <div>
    <ul>
        {notes.map((note)=>{return<li key={note.id} data-testid="notes-list-item">{note.title}</li>})}
    </ul>
   </div>
  )
}

export default NoteList