import { render, screen, waitForElementToBeRemoved } from "@testing-library/react"
import NoteList from "./index"
import { rest } from 'msw'
import { setupServer } from 'msw/node'


const server = setupServer(
    rest.get("/notes-list", (req, res, ctx) => {
        return res(ctx.json({
            notes: [
                {
                    id: 0,
                    title: "lorem React",
                    desc: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
                },
                {
                    id: 1,
                    title: "lorem React notes",
                    desc: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
                }
            ]
        }))
    })
)


describe("Given Node List", () => {

    beforeAll(() => {
        server.listen();
    })

    afterEach(() => {
        server.resetHandlers();
    })

    afterAll(() => {
        server.close();
    })

    test("WHEN notes component is mount THEN render list of notes ", async () => {
        render(<NoteList />);

       await waitForElementToBeRemoved(()=> screen.getAllByTestId("loader"))

       const notesss=screen.getAllByTestId("notes-list-item")
        const noteText=notesss.map((note)=>{
        return note.textContent})
        
        console.log(noteText);
        
        expect(noteText).toEqual([ 'lorem React', 'lorem React notes' ])
    })
})