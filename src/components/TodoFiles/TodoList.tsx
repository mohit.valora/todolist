import React, { useEffect } from 'react'
import { TodoBodyCss } from '../TodoCss/TodoBodyCss'


interface todoList{
    inputLists:string[],
    leftTask:number,
    setLeftTask:(value: number) => void,
}

export const TodoList = ({leftTask ,setLeftTask ,inputLists}:todoList) => {

  useEffect(() => {
    if (inputLists[0] == "") {
      setLeftTask(0)
    } else {
      setLeftTask(leftTask+1)
    }
  }, [inputLists])

  const handleClick = (event:React.ChangeEvent<any>) => {
    if (event.target.style.textDecoration) {
      event.target.style.removeProperty('text-decoration');
      setLeftTask(leftTask + 1);
    } else {
      event.target.style.setProperty('text-decoration', 'line-through');
      leftTask == 0 ? setLeftTask(0) : setLeftTask(leftTask - 1);
    }
  };
  return (
    <>
      <TodoBodyCss>
        <div className="TodoList">
          {inputLists.map((list:string, index:number) => {
            return (!(list == "") && (<li onClick={handleClick} data-testid="listElement" key={index}>{list}</li>))
          })}
        </div>
      </TodoBodyCss>
    </>
  )
}

