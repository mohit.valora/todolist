import React, { FC, useState, useContext } from 'react'
import { TodoFooterCss } from "../TodoCss/TodoFooterCss";

interface leftTasktype {
    leftTask: number
}

export const TodoFooter: FC<leftTasktype> = ({ leftTask }) => {

    return (
        <TodoFooterCss>
            <div className="TodoFooter">
                <h5 style={{opacity:.5}}>{`${leftTask} ${leftTask == 1 ? "task" : "tasks"} left`} </h5>
            </div>
        </TodoFooterCss>
    )
}