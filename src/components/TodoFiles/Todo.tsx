import React, { useState } from 'react'
import { TodoFooter } from './TodoFooter'
import { TodoInput } from './TodoInput'
import { TodoList } from './TodoList'

export const Todo = () => {
  const [inputLists, setInputLists] = useState([""])
  const [leftTask, setLeftTask] = useState(0) 
  return (
    <>
      <TodoInput inputLists={inputLists} setInputLists={setInputLists} />
      <TodoList leftTask={leftTask} setLeftTask={setLeftTask} inputLists={inputLists} />
      <TodoFooter leftTask={leftTask} />
    </>
  )
}

