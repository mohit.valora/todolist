import React, { useState } from 'react'
import { TodoHeaderCss } from '../TodoCss/TodoHeaderCss'

interface todoInput {
    inputLists: string[],
    setInputLists: (value: string[]) => void,
}

export const TodoInput = ({ inputLists, setInputLists }: todoInput) => {
    const [inputList, setInputList] = useState("")
    const addList = () => {
        if (inputList != "") {

            if (inputLists[0] == "") {
                setInputLists([inputList])
            } else {
                setInputLists([...inputLists, inputList])
            }
            setInputList("")
        }
    }

    return (
        <>
            <TodoHeaderCss>
                <div className="main">
                    <div className="inputAdd">
                        <input type="text" value={inputList} onChange={(e) => { setInputList(e.target.value) }} required placeholder="Add a new task here ..." />
                    </div>
                    <div className="btnAdd">
                        <button onClick={() => { addList() }} name="add">+</button>
                    </div>
                </div>
            </TodoHeaderCss>
        </>
    )
}


