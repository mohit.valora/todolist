import React,{FC} from 'react'

interface TitleType{
    title:string;
} 

export const TodoHeading:FC<TitleType> = ({title}) => {
    return (
                <h1 title='heading' data-testid="heading">{title}</h1>
    )
}

