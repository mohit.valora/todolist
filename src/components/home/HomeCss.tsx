import React from "react";
import styled from "styled-components";

export const HomeCss = styled.div`
  padding: 0;
  margin: 0;
  background: black;
  height: 100vh;
  h1 {
    margin: 0;
    margin-left:25rem;
    padding-top:4rem;
    color: #ffc65c;
    font-Size:65px;
    letter-spacing:10px;
    font-weight:700;
  }
`;
