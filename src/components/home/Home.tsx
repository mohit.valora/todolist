import React from 'react'
import { Todo } from '../TodoFiles/Todo'
import {TodoHeading} from '../TodoFiles/TodoHeading'
import { HomeCss } from './HomeCss'

export const Home = () => {
    return (
        <>
            <HomeCss>
                <TodoHeading title="TODO" />
                <Todo/>
            </HomeCss>
        </>
    )
}
