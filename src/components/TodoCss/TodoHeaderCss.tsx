import React from "react";
import styled from "styled-components";

export const TodoHeaderCss = styled.div`
  display: inline-block;
  .main {
    display: flex;
  }
  .inputAdd {
    input {
      border-radius:5px 0 0 5px;
      margin-left: 25rem;
      margin-top: 2rem;
      width: 69.5rem;
      font-size: 15px;
      height: 3rem;
      padding-left: 10px;
      border: none;
    }
    input:focus {
      outline: none;
    }
  }
  .btnAdd {
    background-color: white;
    display: inline-block;
    margin-top: 2rem;
    padding: 0.5rem;
    border-radius: 0 5px 5px 0;
  }
  button {
    background-color: #ffc65c;
    border-radius:5px;
    border:none;
    font-size:25px;
    flex: inline;
    color:#000000;
    width: 5rem;
    height: 2rem;
    margin: 0rem;
  }
  button:hover{
    background-color: #f9ab1c;
    width: 5.1rem;
    height: 2.1rem;
  }
`;
