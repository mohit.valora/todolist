import React from "react";
import styled from "styled-components";

export const TodoFooterCss= styled.div`
    .TodoFooter{
        width: 76rem;
        height:5rem;
        background-color:white;
        margin-top:2rem;
        margin-left:25rem;
        border-radius:5px;
        display:flex;
    }
    h5{
        font-size:18px;
        padding-left:18px;
        justify-content:center;
    }
`