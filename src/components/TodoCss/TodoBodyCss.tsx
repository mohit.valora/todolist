import React from 'react'
import styled from 'styled-components'
export const TodoBodyCss = styled.div`
    .TodoList{
        width: 76rem;
        height:25rem;
        background-color:white;
        margin-top:2rem;
        margin-left:25rem;
        border-radius:5px;
        li{
            padding:10px;
            margin:5px;
            border-bottom:1.2px solid #a0a0a0;
        }
        li:hover{
            background-color:#eeeeee
        }
        .line-through{
            text-decoration: line-through;            }
    }
`
